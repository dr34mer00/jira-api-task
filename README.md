### Jira Api Client

This is a simple Jira Api Client written in Java (Apache Http Component, Mockito, Junit). 
It is based on the [Jira Project](https://patkonrekrut.atlassian.net/).

### How to run

1. Clone the repository
2. Set Jira credentials
3. Run `mvn clean install`
4. Run with your IDE

* Optional you can run test `mvn test`




