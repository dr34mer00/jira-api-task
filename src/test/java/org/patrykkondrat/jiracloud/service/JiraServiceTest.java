package org.patrykkondrat.jiracloud.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.patrykkondrat.jiracloud.model.JiraResponse;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class JiraServiceTest {

    private JiraService jiraService = new JiraService();

    @Test
    void testGetIssueKeySummary() {
        String json = "{\"total\": 2, \"issues\": [{\"key\": \"JIRA-123\", \"fields\": {\"summary\": \"Summary 1\"}}, " +
                "{\"key\": \"JIRA-456\", \"fields\": {\"summary\": \"Summary 2\"}}]}";

        List<JiraResponse> jiraResponses = jiraService.getIssueKeySummary(json);

        assertEquals(2, jiraResponses.size());
        assertEquals("JIRA-123", jiraResponses.get(0).key());
        assertEquals("Summary 1", jiraResponses.get(0).summary());
        assertEquals("JIRA-456", jiraResponses.get(1).key());
        assertEquals("Summary 2", jiraResponses.get(1).summary());
    }

    @Test
    public void testGetIssueKeySummary_JsonProcessingException() {
        String json = "invalid json";

        assertThrows(RuntimeException.class, () -> jiraService.getIssueKeySummary(json));
    }

    @Test
    public void testGetJiraJson() {
        String insideJson = "issues";

        String jsonResponse = jiraService.getJiraJson("/rest/api/3/search");

        assertTrue(jsonResponse.toLowerCase().contains(insideJson));
    }
}