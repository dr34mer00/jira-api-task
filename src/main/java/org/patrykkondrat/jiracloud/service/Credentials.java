package org.patrykkondrat.jiracloud.service;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Credentials {

    public Map<String,String> readCredentials() {
        String fileName = "credits";
        Map<String,String> credits = new HashMap<>();
        try (InputStream inputStream = Credentials.class.getResourceAsStream("/" + fileName)) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] split = line.split("=", 2);
                credits.put(split[0], split[1]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return credits;
    }
}
