package org.patrykkondrat.jiracloud.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.patrykkondrat.jiracloud.model.JiraResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JiraService {
    private ObjectMapper objectMapper = new ObjectMapper();

    public String getJiraJson(String apiEndpoint) {
        HttpClient httpClient = HttpClientBuilder.create().build();

        Credentials credentials = new Credentials();
        Map<String, String> credits = credentials.readCredentials();

        String authToken = getAuthToken(credits.get("username"), credits.get("api-token"));

        HttpGet httpGet = new HttpGet(credits.get("jira-url") + apiEndpoint);
        httpGet.setHeader(HttpHeaders.AUTHORIZATION, "Basic " + authToken);

        try {
            HttpResponse response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            return EntityUtils.toString(entity);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private String getAuthToken(String username, String apiToken) {
        String credentials = username + ":" + apiToken;
        byte[] credentialsBytes = credentials.getBytes();
        return java.util.Base64.getEncoder().encodeToString(credentialsBytes);
    }

    public List<JiraResponse> getIssueKeySummary(String json) {
        List<JiraResponse> jiraResponses = new ArrayList<>();
        try {
            JsonNode jsonNode = objectMapper.readTree(json);
            int issuesCount = jsonNode.get("total").asInt();

            for (int i = 0; i < issuesCount; i++) {
                JiraResponse jiraResponse = new JiraResponse(jsonNode.get("issues").get(i).get("key").asText(),
                        jsonNode.get("issues").get(i).get("fields").get("summary").asText());
                jiraResponses.add(jiraResponse);
            }
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e.getMessage());
        }
        return jiraResponses;
    }
}