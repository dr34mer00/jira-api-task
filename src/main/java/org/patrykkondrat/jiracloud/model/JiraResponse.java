package org.patrykkondrat.jiracloud.model;

public record JiraResponse(String key, String summary) {
    @Override
    public String toString() {
        return "Key: " + key + ", Summary: " + summary;
    }
}
