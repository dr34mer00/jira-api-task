package org.patrykkondrat.jiracloud;

import org.patrykkondrat.jiracloud.model.JiraResponse;
import org.patrykkondrat.jiracloud.service.JiraService;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        JiraService jiraService = new JiraService();
        String json = jiraService.getJiraJson("/rest/api/3/search");
        List<JiraResponse> response = jiraService.getIssueKeySummary(json);

        response.forEach(System.out::println);
    }
}